/**
* Compile with: g++ -std=c++11 worksheet.cpp -o worksheet
* Run with    : ./worksheet
**/
# include <vector>
# include <random>
# include <cmath>
# include <iostream>
# include <chrono>
# include <climits>


using namespace std;
using namespace std :: chrono;

// Return a random int value inclusive
int random_int(int start, int stop){
	static mt19937 generator(random_device{}()); // gets initialized only once on first call of RandomIndex
	uniform_int_distribution<int> distribution(start, stop);
	return distribution(generator);
}


bool check_vector_sorted(const vector<int>& v){
    // check whether all the values in v are in sorted order
    // i.e., if i < j then v[i] <= v[j]

    // TODO: implement this function.
    return false;
}

void insertion_sort(vector<int>& x){
    // TODO : implement insertion sort
}

void merge_sort(vector<int>& x){
   // TODO : implement merge sort
}


void rand_quick_sort(vector<int>& x){
    // TODO: Implement randquicksort
}

double find_median_slow(const vector<int>& x){
    // make a copy of x
    vector<int> copy_of_x(x);

    // sort the copy of x
    merge_sort(copy_of_x);

    // return the middle value of the sorted vector
    // even case
    if (x.size() % 2 == 0){
        return (static_cast<double>(copy_of_x[x.size()/2] + copy_of_x[x.size()/2 - 1]))/2;
    }
    // odd case
    return copy_of_x[x.size()/2];
}

int find_median_fast(const vector<int>& x){
    //find_median_slow(.) runs in O(n log n).
    //TODO: find the median in an expected time of O(n).
    //HINT: modify rand_quick_sort(.)
    return 0;

}

/**
* an inversion in a sequence x happens when 
* index i < index j but x[i] > x[j].
*
* Once you've implemented count_inversions_fast(.) you could
* modify main() to run an experiment verifying that insertion_sort(.)
* runs in a time proportional to the number of inversions but merge_sort(.)'s
* run time is independent of the number inversions.
**/

int count_inversions_slow(const vector<int>& x){
    int count{0};
    for (int i = 0; i < x.size(); i++){
        for (int j = i+1; j < x.size(); j++){
            if (x[i] > x[j]){
                count++;
            }
        }
    }
    return count;
}

int count_inversions_fast(const vector<int>& x){
    // count_inversions_slow(.) counts the inversions in x in O(n^2) time.
    // TODO: implement count_inversions_fast(.) that counts inversions in x
    // but in O(n log n) time. HINT: modify merge sort.
    return -1;
}

void print_vector(const vector<int>& v){
        int head = 6;
        int tail = 6;
        int full_print = 20;
        if (v.size() <= full_print){
            for (int a : v){
                cout << a << " ";
            }
            cout << endl;
        } else {
            for (int i = 0; i < head; i++){
                cout << v[i] << " ";
            }
            cout << "... ";
            for (int i = v.size() - tail; i < v.size(); i++){
                cout << v[i] << " ";
            }
            cout << "(" << v.size() << " elements in vector)" << endl;
        }
    }


int main(){
    // The sorting functions and median function:

    // Create a random vector.
    int rv_length = 50000;
    int rv_lb = -500000;
    int rv_ub = 500000;
    vector<int> random_vector(rv_length);
    for (int& a : random_vector){
        a = random_int(rv_lb, rv_ub);
    }
    cout << "The random vector: ";
    print_vector(random_vector);

    // Confirm that it's not in sorted order:
    string status = (check_vector_sorted(random_vector)) ? "sorted" : "not sorted";
    cout << "This vector is " << status << endl << endl;

    // INSERTION SORT
    // create a copy of random vector to be sorted by insertion sort
    vector<int> rv_is(random_vector);
    auto start_time = high_resolution_clock :: now();  
    insertion_sort(rv_is);
    auto end_time = high_resolution_clock :: now();
    auto int_ms =  duration_cast<milliseconds>(end_time - start_time);
    cout << "Sorting with INSERTION SORT took " << int_ms.count() << "ms. ";
    status = (check_vector_sorted(rv_is)) ? "SORTED" : "NOT SORTED";
    cout << "The vector was " << status << ". ";
    print_vector(rv_is);

    // MERGE SORT
    // create a copy of random vector to be sorted by merge sort
    vector<int> rv_ms(random_vector);
    start_time = high_resolution_clock :: now();  
    merge_sort(rv_ms);
    end_time = high_resolution_clock :: now();
    int_ms =  duration_cast<milliseconds>(end_time - start_time);
    cout << "Sorting with MERGE SORT took " << int_ms.count() << "ms. ";
    status = (check_vector_sorted(rv_ms)) ? "SORTED" : "NOT SORTED";
    cout << "The vector was " << status << ". ";
    print_vector(rv_ms);

    // RAND QUICK SORT
    // create a copy of random vector to be sorted by rand quick sort
    vector<int> rv_rqs(random_vector);
    start_time = high_resolution_clock :: now();  
    rand_quick_sort(rv_rqs);
    end_time = high_resolution_clock :: now();
    int_ms =  duration_cast<milliseconds>(end_time - start_time);
    cout << "Sorting with RAND QUICK SORT took " << int_ms.count() << "ms. ";
    status = (check_vector_sorted(rv_rqs)) ? "SORTED" : "NOT SORTED";
    cout << "The vector was " << status << ". ";
    print_vector(rv_rqs);

    // FIND MEDIAN SLOW
    start_time = high_resolution_clock :: now();  
    int median = find_median_slow(random_vector);
    end_time = high_resolution_clock :: now();
    int_ms =  duration_cast<milliseconds>(end_time - start_time);
    cout << "Finding median with FIND MEDIAN SLOW takes " << int_ms.count() << "ms.";
    cout << "The median was found to be " << median << "." << endl;

    // FIND MEDIAN FAST
    start_time = high_resolution_clock :: now();  
    median = find_median_fast(random_vector);
    end_time = high_resolution_clock :: now();
    int_ms =  duration_cast<milliseconds>(end_time - start_time);
    cout << "Finding median with FIND MEDIAN FAST takes " << int_ms.count() << "ms.";
    cout << "The median was found to be " << median << "." << endl;
    
}



